# Documenting Requirements

1. Create a new issue for each work item: [Epic, Stories, Tasks](https://pm.stackexchange.com/questions/16739/what-is-the-weighting-difference-between-epic-story-task?newreg=9ecfe96250b344caa0ad4ed75a5266af).
2. Set label for `Epic`, `Story`, `Task`
3. Link Epic issue, with Story issue, with Task issue from inside the description of each issue. Using [Task lists feature of gitlab](https://docs.gitlab.com/ee/user/markdown.html#task-lists)
4. Ensure [User Story does only 1 thing](#5)
5. Reporting bugs : Either create a new issue and label it as `Bug`. Or reopen an existing work item above
6. [WHY, WHAT, HOW](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/documenting-requirements/-/issues/1) in issue tracker. [See webinar](https://web.microsoftstream.com/video/94e9bd2c-8346-4c3f-a944-cf4a7029a4a0)
7. Document requirements in README.md markdown file. In there Link to important issues (epics for example), and link to other markdown files

# See Also
Pre-requisite to Documenting requirements: [Defining Requirements](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/defining-requirements)
